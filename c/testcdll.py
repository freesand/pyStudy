"""testcdll.py Python使用C编写的共享库
"""
import os
from ctypes import CDLL
if os.name == "posix":  # *nix
    cdll = CDLL(os.path.abspath("libmylib.so"))
else:  # Windows
    cdll = CDLL(os.path.abspath("mylib.dll"))
result = cdll.accumulate(500)
print(result)
