# -*- coding: utf-8 -*-
"""turtledraw.pyw 自定义的海龟绘图函数集
"""
import turtle as tt


def star5p(x, y, size=20, angle=0):
    """在指定位置画一颗五角星
    """
    t = tt.Turtle()  # 生成一个单独的海龟对象
    t.hideturtle()
    t.speed(0)
    t.color("white")
    t.penup()
    t.setpos(x, y)
    t.right(angle)
    t.begin_fill()  # 启用填充区域
    cnt = 0
    while cnt < 5:
        t.forward(size)
        t.left(72)
        t.forward(size)
        t.right(144)
        cnt += 1
    t.end_fill()


def test():
    """测试绘图函数：随机画十颗五角星
    """
    from random import randint
    tt.TurtleScreen._RUNNING = True
    tt.setup(width=720, height=480, startx=None, starty=None)
    tt.hideturtle()
    tt.speed(0)
    tt.bgcolor("purple")
    tt.penup()
    cnt = 0
    while cnt < 10:
        x = randint(-300, 300)
        y = randint(-200, 200)
        s = randint(10, 30)
        a = randint(0, 72)
        star5p(x, y, s, a)  # 带参数调用五角星函数
        cnt += 1
    tt.done()


if __name__ == "__main__":  # 运行模块时调用测试绘图函数
    test()
