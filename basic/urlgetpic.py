# -*- coding: utf-8 -*-
"""urlgetpic.py “岁月小筑”随机图片API测试程序
获取一张随机图片的网址并用浏览器打开
"""
from urllib.request import urlopen
import webbrowser
url = "http://img.xjh.me/random_img.php?return=url"


def main():
    try:
        pic = urlopen(url).read().decode()
        webbrowser.open("http:" + pic)
    except Exception as e:
        print(repr(e))


if __name__ == "__main__":
    main()
