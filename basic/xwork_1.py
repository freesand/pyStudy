"""xwork_1.py 主程序依次执行多个任务
"""
import time


def work(tasknum):
    t1 = time.perf_counter()
    print(f"任务{tasknum}开始……")
    time.sleep(3)
    print(f"任务{tasknum}完成！耗时{time.perf_counter() - t1}秒。")


def main():
    work(1)
    work(2)


if __name__ == "__main__":
    t1 = time.perf_counter()
    main()
    print(f"主程序耗时{time.perf_counter() - t1}秒。")
