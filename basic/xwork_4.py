"""xwork_4.py 多个协程并发执行多个任务
"""
import time
import asyncio


async def work(tasknum):
    t1 = time.perf_counter()
    print(f"任务{tasknum}开始……")
    await asyncio.sleep(3)
    print(f"任务{tasknum}完成！耗时{time.perf_counter() - t1}秒。")


async def main():
    tasks = asyncio.gather(work(1), work(2))
    await tasks


if __name__ == "__main__":
    t1 = time.perf_counter()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    # 在Spyder中用下面这句才行，因为IPyhon已启动事件循环
    # asyncio.run_coroutine_threadsafe(main(), loop)
    # 在Python 3.7中用下面这句即可，不必再去获取事件循环
    # asyncio.run(main())
    print(f"主程序耗时{time.perf_counter() - t1}秒。")
