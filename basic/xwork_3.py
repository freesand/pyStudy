"""xwork_3.py 多个线程并发执行多个任务
"""
import time
import threading
import sys


def work(tasknum):
    t1 = time.perf_counter()
    # print()在多线程下会有问题，可用sys.stdout.write()代替
    sys.stdout.write(f"任务{tasknum}开始……\n")
    time.sleep(3)
    sys.stdout.write(f"任务{tasknum}完成！耗时{time.perf_counter() - t1}秒。\n")


def main():
    threading.Thread(target=work, args=(1,)).start()
    threading.Thread(target=work, args=(2,)).start()


if __name__ == "__main__":
    t1 = time.perf_counter()
    main()
    print(f"主程序耗时{time.perf_counter() - t1}秒。")
