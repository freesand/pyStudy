# -*- coding: utf-8 -*-
"""colorsfile.py 生成颜色列表
"""
rgb_path = "rgb.txt"
colors = []
with open(rgb_path) as file:  # 打开文件
    for line in file:  # 每次读取一行
        if not (line.isspace() or line.startswith("!")):  # 空行或!打头的行则不必处理
            # 最后一个制表符之后的字符串 split("\t")[-1]
            # 再去掉末尾的换行符 [:-1] 就是颜色名
            # 把颜色名添加进列表即可
            colors.append(line.split("\t")[-1][:-1])
for i in colors[:]:  # 推荐这样返回一个新列表作为迭代器，否则原列表改变会影响循环次数
    if " " in i or "grey" in i.lower():
        colors.remove(i)
file = open("colorslist.py", "w")  # 以写入模式打开文件
file.write(str(colors))  # 列表转为字符串并写入文件
file.close()  # 如果打开文件未用with语句，则要记得关闭文件
