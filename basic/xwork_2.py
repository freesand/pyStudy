"""xwork_2.py 多个进程并发执行多个任务
"""
import time
import multiprocessing


def work(tasknum):
    t1 = time.perf_counter()
    print(f"任务{tasknum}开始……")
    time.sleep(3)
    print(f"任务{tasknum}完成！耗时{time.perf_counter() - t1}秒。")


def main():
    p1 = multiprocessing.Process(target=work, args=(1,))
    p2 = multiprocessing.Process(target=work, args=(2,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()


if __name__ == "__main__":
    t1 = time.perf_counter()
    main()
    print(f"主程序耗时{time.perf_counter() - t1}秒。")
