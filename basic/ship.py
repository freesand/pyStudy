# -*- coding: utf-8 -*-
"""ship.py 船的家族
"""


class Ship:
    """船类"""
    def __init__(self, name=None):
        """初始化船实例"""
        self.name = name  # 船名
        self.crew = 0  # 船员人数

    def join(self, number):
        """船员加入"""
        self.crew += number
        return self.crew


class Warship(Ship):
    """战舰类"""
    def __init__(self, name=None, level=None):
        super().__init__(name)  # 先调用基类初始化方法
        self.level = level  # 舰级


if __name__ == "__main__":
    ws1 = Warship("蓝色空间", "恒星级")
    ws1.join(500)
    print("{}战舰{}号，现有舰员{}人。".format(ws1.level, ws1.name, ws1.crew))
