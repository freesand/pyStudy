# -*- coding: utf-8 -*-
"""colorstable.pyw 显示颜色列表中的所有颜色
"""
import turtle as tt


def showcolor(x, y, color="black"):
    """显示颜色块和颜色名
    """
    t = tt.Turtle()
    t.hideturtle()
    t.speed(0)
    t.color(color)
    t.penup()
    t.setpos(x, y)
    t.begin_fill()
    for _ in range(4):
        t.forward(16)
        t.left(90)
    t.forward(18)
    t.end_fill()
    t.color("black")
    t.write(color)


def showcolors():
    """显示所有颜色
    """
    from colorslist import COLORS
    tt.TurtleScreen._RUNNING = True
    tt.setup(1200, 820)
    tt.setworldcoordinates(0, -810, 1200, 10)
    tt.hideturtle()
    tt.speed(0)
    tt.tracer(0)  # 关闭动效以减少耗时
    tt.penup()
    row = 0
    col = 0
    for color in COLORS:
        showcolor(col * 100, row * -18, color)
        row += 1
        if row > 45:
            row = 0
            col += 1
    tt.done()


if __name__ == "__main__":
    showcolors()
