# -*- coding: utf-8 -*-
"""accumulate.py 计算1累加至n的结果
"""
# 获取输入的字符串，转换为整数，赋值给变量n
n = int(input("计算1累加至n，请输入n："))
x = 1  # 变量x赋值1
result = 0  # 变量result赋值0
while x <= n:  # 当x小于等于n时循环执行子语句
    result += x  # result原始加x
    x += 1  # x原值加1
print("1累加至{}的结果是{}".format(n, result))  # 输出包含n和result的字符串
