#include <iostream>
const double PI = 3.14159;
class Circle
{
  private:
    double r;

  public:
    Circle(double r_)
    {
        r = r_;
    }
    double area()
    {
        return PI * r * r;
    }
    double perimiter()
    {
        return 2 * PI * r;
    }
};
int main()
{
    double r;
    std::cout << "请输入圆的半径：";
    std::cin >> r;
    Circle c = Circle(r);
    std::cout << "圆的面积为：" << c.area() << std::endl;
    std::cout << "圆的周长为：" << c.perimiter() << std::endl;
}
